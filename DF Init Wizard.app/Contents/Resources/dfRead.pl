#!/usr/bin/perl

# use strict;
# use warnings;

# data utility for dwarf fortress init wizard
# reads values from init.txt and d_init.txt

&myusage unless ($ARGV[0] && $ARGV[1]);

my $initFile = $ARGV[0];
my $d_initFile = $ARGV[1];

my @init_values = ("SOUND",
	"VOLUME",
	"INTRO",
	"WINDOWED",
	"FPS",
	"FPS_CAP",
	"G_FPS_CAP",
	"PRIORITY",
	"ZOOM_SPEED",
	"MOUSE",
	"MACRO_MS",
	"COMPRESSED_SAVES");

my @d_init_values = ("AUTOSAVE",
	"AUTOBACKUP",
	"AUTOSAVE_PAUSE",
	"INITIAL_SAVE",
	"PAUSE_ON_LOAD",
	"EMBARK_WARNING_ALWAYS",
	"TEMPERATURE",
	"WEATHER",
	"ECONOMY",
	"INVADERS",
	"CAVEINS",
	"ARTIFACTS",
	"LOG_MAP_REJECTS",
	"EMBARK_RECTANGLE",
	"COFFIN_NO_PETS_DEFAULT",
	"IDLERS",
	"SET_LABOR_LISTS",
	"POPULATION_CAP",
	"BABY_CHILD_CAP",
    "VARIED_GROUND_TILES",
	"ENGRAVINGS_START_OBSCURED",
	"SHOW_IMP_QUALITY",
	"SHOW_FLOW_AMOUNTS",
	"NICKNAME_DWARF",
	"NICKNAME_ADVENTURE",
	"NICKNAME_LEGENDS");

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

open INPUT, "<$initFile" or die $!;
my @lines = <INPUT>;
close INPUT;

foreach my $line (@lines) {
	
	chomp($line); # remove the newline char
	
	foreach my $value (@init_values) {
		if ($line =~ /^\[$value\:/) {
			&getLineSetting($line);
		}
	}
	
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

open INPUT, "<$d_initFile" or die $!;
my @lines = <INPUT>;
close INPUT;

foreach my $line (@lines) {
	
	chomp($line); # remove the newline char
	
	foreach my $value (@d_init_values) {
		if ($line =~ /^\[$value\:/) {
			&getLineSetting($line);
		}
	}
	
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # SUBROUTINES # # # # # # # # # # # # # # # # #

sub getLineSetting {
	my $input = $_[0];
	$input =~ s/\[//g;  # remove the leading bracket
	$input =~ s/\]//g;  # remove the ending bracket
	$input =~ s/\r//g;  # remove the ending bracket
	$input =~ s/\n//g;  # remove the ending bracket	
	my @parts = split(/\:/, $input);	# split by colon :
	shift(@parts); # remove the option name
	$option = join(':',@parts);	# join multiple option components back together (if any)
	print $option;
	print " ";
}

sub myusage {
	print "\n\nRun me again with a DF init.txt and d_init.txt as arguments.\n\n";
	exit;
}