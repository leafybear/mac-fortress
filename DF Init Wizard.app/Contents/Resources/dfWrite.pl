#!/usr/bin/perl

# use strict;
# use warnings;

# data utility for dwarf fortress init wizard
# writes to init.txt and d_init.txt

&myusage unless ($ARGV[0] && $ARGV[1] && $ARGV[2]);

my $initFile = $ARGV[0];
my $d_initFile = $ARGV[1];

my @init_values = ("SOUND",
	"VOLUME",
	"INTRO",
	"WINDOWED",
	"FPS",
	"FPS_CAP",
	"G_FPS_CAP",
	"PRIORITY",
	"ZOOM_SPEED",
	"MOUSE",
	"MACRO_MS",
	"COMPRESSED_SAVES");

my @d_init_values = ("AUTOSAVE",
	"AUTOBACKUP",
	"AUTOSAVE_PAUSE",
	"INITIAL_SAVE",
	"PAUSE_ON_LOAD",
	"EMBARK_WARNING_ALWAYS",
	"TEMPERATURE",
	"WEATHER",
	"ECONOMY",
	"INVADERS",
	"CAVEINS",
	"ARTIFACTS",
	"LOG_MAP_REJECTS",
	"EMBARK_RECTANGLE",
	"COFFIN_NO_PETS_DEFAULT",
	"IDLERS",
	"SET_LABOR_LISTS",
	"POPULATION_CAP",
    "BABY_CHILD_CAP",
    "VARIED_GROUND_TILES",
	"ENGRAVINGS_START_OBSCURED",
	"SHOW_IMP_QUALITY",
	"SHOW_FLOW_AMOUNTS",
	"NICKNAME_DWARF",
	"NICKNAME_ADVENTURE",
	"NICKNAME_LEGENDS");

my $newInit = "";
my $newDinit = "";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #		I N I T

open INPUT, "<$initFile" or die $!;
my @lines = <INPUT>;
close INPUT;

foreach my $line (@lines) {
	
	chomp($line); # remove the newline char for easier processing
	
	# keep track of which value so we can match with the right argument
	my $i = 2;
	foreach my $value (@init_values) {
		if ($line =~ /^\[$value\:/) {
			$line = "[" . $value . ":" . $ARGV[$i] . "]";	
			print $line;
		}
		$i++;
	}

	$newInit = $newInit . $line . "\n";	# append the changed (or original) line
}

open OUTPUT, ">$initFile" or die $!;
print OUTPUT $newInit;
close OUTPUT;

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #		D - I N I T

open DINPUT, "<$d_initFile" or die $!;
my @dlines = <DINPUT>;
close DINPUT;

foreach my $line (@dlines) {
	
	chomp($line); # remove the newline char for easier processing
	
	# keep track of which value so we can match with the right argument
	my $i = 14;
	foreach my $value (@d_init_values) {
		if ($line =~ /^\[$value\:/) {
            if ($i > 37) { $i = 37; }
			$line = "[" . $value . ":" . $ARGV[$i] . "]";	
			print $line;
		}
		$i++;
	}

	$newDinit = $newDinit . $line . "\n";	# append the changed (or original) line
}

open DOUTPUT, ">$d_initFile" or die $!;
print DOUTPUT $newDinit;
close DOUTPUT;

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # SUBROUTINES # # # # # # # # # # # # # # # # #

sub myusage {
	print "\n\nRun me again with a DF init.txt and d_init.txt as arguments (in that order) followed by the new value for all the options available.\n\n";
	exit;
}