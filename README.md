Mac Fortress
====================

Super-helpful bundle of tools for mac users playing [Dwarf Fortress](http://www.bay12games.com/dwarves/).

### DF Launcher

The df binary for mac can't go in the dock (or in steam). That's a bummer. So put this applet in your DF install folder and you can run it instead. (Icon from Jolly Bastion theme.)

### QuickFort droplet

A droplet front end for the spreadsheet driven construction tool, [Quickfort2](http://www.joelpt.net/quickfort/). Drag a folder or individual csv/xls files onto the droplet to get the .mak files out. The droplet with use qfconvert to convert them to DF's macro format and name them to that you can pick them by name in the DF macro menu. Point it at your DF install and it will put the macros in the right folder.

This droplet includes the current version of qfconvert from [Quickfort2](http://www.joelpt.net/quickfort/). [Quickfort2](http://www.joelpt.net/quickfort/) is a fantastic tool for creating macros to use in game from csv or xls files. The source page for QuickFort is located [here](https://github.com/joelpt/quickfort) on GitHub.

### Switch Graphics droplet

Drag-and-drop graphics set changer for the mac Dwarf Fortress client. A standard graphics folder (containing data, raw folders) can just be dropped onto the app to install to your DF. It will change save folders to the new raws if any. If you want to change from graphics to ascii, it will work best to include original DF raw folder contents in your tileset's folder. So example folders would be:

	/my graphics set
		/data
			/art
			/init
		/raw
			.... whatever your set has


### DF Init Wizard

A tools to change your init/dinit settings easily. Just run the app, choose your DF folder, and off you go! The Dwarf Fortress folder you use will have to have all the proper files in data/init or else the wizard won't work. Choose the settings you want and click save. That's all there is to it. It will work on any DF install folder (mac/windows/linux) as long as you're running this on a mac.

![DF Init Wizard screenshot](https://raw.github.com/fidgetfu/Mac-Fortress/master/wizard.png)